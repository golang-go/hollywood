package main

import (
	"log"

	"github.com/anthdm/hollywood/actor"
	"github.com/anthdm/hollywood/cluster"
	"github.com/anthdm/hollywood/remote"
)

type Inventory struct{}

func NewInventory() actor.Producer {
	return func() actor.Receiver {
		return &Inventory{}
	}
}

func (i *Inventory) Receive(c *actor.Context) {

}

func main() {
	remote := remote.New("127.0.0.1:3001", nil)
	e, err := actor.NewEngine(&actor.EngineOpts{Remote: remote})
	if err != nil {
		log.Fatal(err)
	}
	memberAddr := cluster.MemberAddr{
		ListenAddr: "localhost:3000",
		ID:         "A",
	}
	cfg := cluster.Config{
		ClusterProvider: cluster.NewSelfManagedProvider(memberAddr),
		ID:              "B",
		Region:          "eu-east",
		Engine:          e,
	}
	c, err := cluster.New(cfg)
	if err != nil {
		log.Fatal(err)
	}
	c.RegisterKind("inventory", NewInventory(), nil)
	if err := c.Start(); err != nil {
		log.Fatal(err)
	}
	c.Activate("inventory", &cluster.ActivationConfig{ID: "1"})
	select {}
}
